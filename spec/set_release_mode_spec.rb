require 'spec_helper'

describe Fastlane::Actions::SetReleaseModeAction do
  describe "Set Release Mode Information from plist" do
    before do
      # # Create test folder
      # FileUtils.mkdir_p(test_path)
      # source = File.join(fixtures_path, proj_file)
      # destination = File.join(test_path, proj_file)
      #
      # # Copy .xcodeproj fixture, as it will be modified during the test
      # FileUtils.cp_r(source, destination)
    end

    describe "get_release_mode" do
      let (:plist_path) { "./fastlane-plugin-versionista/spec/fixtures/plist/Info.plist" }
      let (:new_value) { "beta-external#{Time.now.to_i}" }

      it "stores release mode in the plist file" do
        old_value = Fastlane::FastFile.new.parse("lane :test do
          get_release_mode(path: '#{plist_path}')
        end").runner.execute(:test)

        Fastlane::FastFile.new.parse("lane :test do
          set_release_mode(path: '#{plist_path}', mode: '#{new_value}')
        end").runner.execute(:test)

        value = Fastlane::FastFile.new.parse("lane :test do
          get_release_mode(path: '#{plist_path}')
        end").runner.execute(:test)

        expect(value).to eq(new_value)

        ret = Fastlane::FastFile.new.parse("lane :test do
          set_release_mode(path: '#{plist_path}', mode: '#{old_value}')
        end").runner.execute(:test)
        expect(ret).to eq(old_value)
      end
    end

    after do
      # Clean up files
      # FileUtils.rm_r(test_path)
    end
  end
end
