# coding: utf-8
lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'fastlane/plugin/versionista/version'

Gem::Specification.new do |spec|
  spec.name          = 'fastlane-plugin-versionista'
  spec.version       = Fastlane::Versionista::VERSION
  spec.author        = %q{thecb4}
  spec.email         = %q{cavelle@thecb4.io}

  spec.summary       = %q{Extended versioning capabilities for fastlane based on Semantic Versioning}
  # spec.homepage      = "https://github.com/<GITHUB_USERNAME>/fastlane-plugin-versionista"
  spec.license       = "MIT"

  spec.files         = Dir["lib/**/*"] + %w(README.md LICENSE)
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ['lib']

  # spec.add_dependency 'your-dependency', '~> 1.0.0'

  spec.add_development_dependency 'pry'
  spec.add_development_dependency 'bundler'
  spec.add_development_dependency 'rspec'
  spec.add_development_dependency 'rake'
  spec.add_development_dependency 'rubocop'
  spec.add_development_dependency 'fastlane', '>= 1.110.0'
end
