module Fastlane
  module Helper
    class VersionistaHelper
      # class methods that you define here become available in your action
      # as `Helper::VersionistaHelper.your_method`
      #
      def self.show_message
        UI.message("Hello from the versionista plugin helper!")
      end
    end
  end
end
