module Fastlane
  module Actions
    module SharedValues
      NEXT_RELASE_BUMP_VALUE = :NEXT_RELASE_BUMP_VALUE
    end

    class GetNextReleaseBumpAction < Action
      def self.run(params)
        require "plist"

        begin
          # path = File.expand_path(params[:path])
          # plist = Plist.parse_xml(path)

          # value = plist[params[:key]]
          # Actions.lane_context[SharedValues::GET_INFO_PLIST_VALUE_CUSTOM_VALUE] = value

          path = File.expand_path(params[:path])
          plist = Plist.parse_xml(path)
          release_bump = plist["NextReleaseBump"]

          # UI.user_error!("release bump = #{release_bump}")
          Actions.lane_context[SharedValues::NEXT_RELASE_BUMP_VALUE] = release_bump

          return release_bump
        rescue => ex
          UI.error(ex)
          UI.error("Unable to find plist file at '#{path}'")
        end
      end

      def self.description
        "Returns Next Release Bump from Info.plist of your project as native Ruby data structures"
      end

      def self.details
        "Get the next release bump from a plist file, which can be used to fetch the app identifier and more information about your app"
      end

      def self.available_options
        [
          # FastlaneCore::ConfigItem.new(key: :key,
          #                              env_name: "FL_GET_INFO_PLIST_PARAM_NAME",
          #                              description: "Name of parameter",
          #                              optional: false),
          FastlaneCore::ConfigItem.new(key: :path,
                                       env_name: "FL_GET_INFO_PLIST_PATH",
                                       description: "Path to plist file you want to read",
                                       optional: false,
                                       verify_block: proc do |value|
                                         UI.user_error!("Couldn't find plist file at path '#{value}'") unless File.exist?(value)
                                       end)
        ]
      end

      def self.output
        [
          ['NEXT_RELASE_BUMP_VALUE', 'The value of the last plist file that was parsed']
        ]
      end

      def self.authors
        ["thecb4"]
      end

      def self.is_supported?(platform)
        [:ios, :mac].include? platform
      end

      def self.example_code
        [
          'next_release_bump = get_next_release_bump(path: "./Info.plist")'
        ]
      end

      def self.category
        :project
      end
    end
  end
end
