module Fastlane
  module Actions
    module SharedValues
    end

    class SetReleaseBumpAction < Action
      def self.run(params)
        require "plist"

        begin
          # path = File.expand_path(params[:path])
          # plist = Plist.parse_xml(path)

          # value = plist[params[:key]]
          # Actions.lane_context[SharedValues::GET_INFO_PLIST_VALUE_CUSTOM_VALUE] = value

          path = File.expand_path(params[:path])
          plist = Plist.parse_xml(path)
          plist["ReleaseBump"] = params[:bump]
          new_plist = Plist::Emit.dump(plist)
          File.write(path, new_plist)

          return params[:bump]
        rescue => ex
          UI.error(ex)
          UI.error("Unable to find plist file at '#{path}'")
        end
      end

      def self.description
        "Sets the Release Bump in the Info.plist of your project as native Ruby data structures"
      end

      def self.details
        "set the release bump in a plist file"
      end

      def self.available_options
        [
          # FastlaneCore::ConfigItem.new(key: :key,
          #                              env_name: "FL_GET_INFO_PLIST_PARAM_NAME",
          #                              description: "Name of parameter",
          #                              optional: false),
          FastlaneCore::ConfigItem.new(key: :path,
                                       env_name: "FL_GET_INFO_PLIST_PATH",
                                       description: "Path to plist file you want to read",
                                       optional: false,
                                       verify_block: proc do |value|
                                         UI.user_error!("Couldn't find plist file at path '#{value}'") unless File.exist?(value)
                                       end),
          FastlaneCore::ConfigItem.new(key: :bump,
                              env_name: "FL_SET_RELEASE_BUMP_VALUE",
                              description: "Value to set in plist",
                              optional: false)
        ]
      end
      #
      # def self.output
      #   [
      #     ['RELASE_BUMP_VALUE', 'The value of the last plist file that was parsed']
      #   ]
      # end

      def self.authors
        ["thecb4"]
      end

      def self.is_supported?(platform)
        [:ios, :mac].include? platform
      end

      def self.example_code
        [
          'set_release_bump(path: "./Info.plist", bump:major)'
        ]
      end

      def self.category
        :project
      end
    end
  end
end
